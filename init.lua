local current_mod_name = minetest.get_current_modname()

flyingships = {}

flyingships.META_CONNECTED = "conn_node"
flyingships.META_BAKED_MOVE = "move_bake"
flyingships.META_BAKED_ROTATE = "rotate_bake"
flyingships.DIRS = {{x=0,y=1,z=0},{x=0,y=0,z=1},{x=0,y=0,z=-1},{x=1,y=0,z=0},{x=-1,y=0,z=0},{x=0,y=-1,z=0}}

flyingships.MAX_BLOCKS_PER_SHIP = minetest.setting_get("flyingships.max_blocks_per_ship")
if flyingships.MAX_BLOCKS_PER_SHIP == nil then
    flyingships.MAX_BLOCKS_PER_SHIP = 4000
end

flyingships.CONFIGURATOR_NODE = core.settings:get("flyingships.configurator")
if flyingships.CONFIGURATOR_NODE == nil then
    flyingships.CONFIGURATOR_NODE = "default:mese_crystal_fragment"
end



dofile(minetest.get_modpath(current_mod_name) .. "/math.lua")
dofile(minetest.get_modpath(current_mod_name) .. "/util.lua")
dofile(minetest.get_modpath(current_mod_name) .. "/functions.lua")
dofile(minetest.get_modpath(current_mod_name) .. "/nodes.lua")
dofile(minetest.get_modpath(current_mod_name) .. "/craft.lua")
